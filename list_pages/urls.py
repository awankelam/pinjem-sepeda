from django.conf.urls import url
from django.urls import include,path
from django.views.generic import RedirectView
from .views import voucher,peminjaman

urlpatterns = [
    url(r'^voucher/', voucher, name='voucher'),
    url(r'^peminjaman/', peminjaman, name='peminjaman'),
]