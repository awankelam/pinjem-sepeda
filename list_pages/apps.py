from django.apps import AppConfig


class DaftarVoucherConfig(AppConfig):
    name = 'daftar_voucher'
