from django.shortcuts import render
from django.db import connection

# Create your views here.
response={}

def voucher(request):
   with connection.cursor() as cursor: 
        cursor.execute("SELECT id_voucher,nama,nilai_poin,deskripsi FROM VOUCHER;") 
        record = cursor.fetchall()
        print("========= List Voucher ==========\n ", record,"\n")

   return render(request,'list_voucher.html',response)

def peminjaman(request):
   with connection.cursor() as cursor: 
        cursor.execute("SELECT * FROM PEMINJAMAN;") 
        record = cursor.fetchall()
        print("========= List Peminjaman ==========\n ", record,"\n")

   return render(request,'list_peminjaman.html',response)