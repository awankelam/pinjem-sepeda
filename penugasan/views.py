from django.shortcuts import render
from django.db import connection
from django.http import HttpResponse
from django.shortcuts import redirect

# Create your views here.
def create_penugasan(request):
    response = {}
    return render(request, 'create_penugasan.html', response)

def list_penugasan(request):
    response = {}
    return render(request, 'list_penugasan.html', response)
    
def update_penugasan(request):
    response = {}
    return render(request, 'update_penugasan.html', response)

def save_penugasan(request):
    response = {}
    if request.method == 'POST':
        nama_petugas = request.POST.get('nama_petugas')
        tanggal_mulai = request.POST.get('tanggal_mulai')
        tanggal_selesai = request.POST.get('tanggal_selesai')
        stasiun = request.POST.get('stasiun')
        response['nama_petugas'] = nama_petugas
        response['tanggal_mulai'] = tanggal_mulai
        response['tanggal_selesai'] = tanggal_selesai
        response['stasiun'] = stasiun
        try:
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO PENUGASAN VALUES ({},{},{},{})".format(nama_petugas,
                    tanggal_mulai,tanggal_selesai,stasiun))
                connection.commit()
                count = cursor.rowcount
                print(count, "record inserted successfully")
                record = cursor.fetchone()
                print(record)
        except (Exception, connection.Error) as error :
            if(connection):
                print("Failed to insert record into mobile table", error)
    return redirect('/penugasan/list')