from django.urls import path
from . import views

urlpatterns=[
	path('add/', views.create_penugasan, name='create_tugas'),
	path('list/', views.list_penugasan, name='list_penugasan'),
	path('submit-penugasan/', views.save_penugasan, name='save_penugasan'),
	path('update/', views.update_penugasan, name='update_penugasan'),
]
