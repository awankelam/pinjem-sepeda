from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.db import connection

response = {}

# Create your views here.
def index(request):
    if request.method == 'POST':
        no_ktp = request.POST.get('no_ktp')
        email = request.POST.get('email')
        response['no_ktp'] = no_ktp
        response['email'] = email
        with connection.cursor() as cursor: 
            cursor.execute("SELECT * FROM PERSON WHERE ktp = '{}' AND nama = '{}';".format(no_ktp, email)) 
            person = cursor.fetchall()
            if person:
                request.session['no_ktp'] = no_ktp
                request.session['email'] = email
                cursor.execute("SELECT * FROM PETUGAS WHERE ktp = '{}'".format(no_ktp))
                petugas = cursor.fetchall()
                if petugas:
                    request.session['is_petugas'] = True
                    return redirect('/peminjaman/add')
                else:
                    cursor.execute("SELECT * FROM ANGGOTA WHERE ktp = '{}'".format(no_ktp))
                    anggota = cursor.fetchall()
                    request.session['is_petugas'] = False
                    return redirect("/")
            else:
                raise Http404("akun belum terdaftar")
    else:
        return render(request, 'login.html', response)