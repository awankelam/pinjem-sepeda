from django.conf.urls import url
from django.urls import include,path
from django.views.generic import RedirectView
from .views import add,update,submit_voucher

urlpatterns = [
    path('add',add),
    path('update',update),
    path('submit_voucher/',submit_voucher)
  #  url(r'^process_insert_value', process_insert_value, name='process_insert_value'),
]