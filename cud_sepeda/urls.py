from django.conf.urls import url
from django.urls import include,path
from django.views.generic import RedirectView
from .views import add,update

urlpatterns = [
    path('add',add),
    path('update',update),
]
