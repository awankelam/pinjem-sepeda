from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
    if request.method =='POST':
        response['nominal'] = request.POST.get('nominal')
        return render(request, 'topup.html', response)
    else:
        return render(request, 'topup.html', response)