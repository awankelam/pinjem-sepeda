"""bike_sharing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', include(('login.urls', 'login'), namespace='login')),
    path('voucher/',include(('cud_voucher.urls','cud_voucher'),namespace='cud_voucher')),
    path('peminjaman/',include(('cud_peminjaman.urls','cud_peminjaman'),namespace='cud_peminjaman')),
    path('lists/',include(('list_pages.urls','list_pages'),namespace='list_pages')),
    path('', include('homepage.urls')),
    path('penugasan/', include('penugasan.urls')),
    path('register/', include(('register.urls', 'register'), namespace='register')),
	path('sepeda/', include('cud_sepeda.urls')),
	path('stasiun/', include('cud_stasiun.urls')),
    path('topup/', include(('topup.urls', 'topup'), namespace='topup')),
    path('acara/', include('acara.urls')),
]