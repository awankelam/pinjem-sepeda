from django.urls import path
from . import views

urlpatterns=[
	path('add/', views.index, name='create_acara'),
    path('update/', views.update_acara, name='update_acara'),
	#path('list/', views.list_acara, name='list_acara'),
]
