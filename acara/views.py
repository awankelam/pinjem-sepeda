from django.shortcuts import render

# Create your views here.
def index(request):
    response = {}
    return render(request, 'create_acara.html', response)

def update_acara(request):
    response = {}
    return render(request, 'update_acara.html', response)