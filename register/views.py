from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.db import connection

# Create your views here.
response = {}

def index(request):
    if request.method == 'POST':
        no_ktp = request.POST.get('no_ktp')
        nama = request.POST.get('nama')
        email = request.POST.get('email')
        tanggal_lahir = request.POST.get('tanggal_lahir')
        no_telp = request.POST.get('no_telp')
        alamat = request.POST.get('alamat')
        if no_telp == None:
            no_telp = 'NULL'
        if alamat == None:
            no_telp = 'NULL'
        try:
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO PERSON VALUES ('{}', '{}', '{}', '{}', '{}', '{}'".format(
                    no_ktp, nama, email, tanggal_lahir, no_telp, alamat))
                connection.commit()
                return render(request, 'registersuccess.html', response)
        except (Exception, connection.Error) as error:
            if (connection):
                raise Http404("gagal meregister akun")
    else:
        return render(request, 'register.html', response)